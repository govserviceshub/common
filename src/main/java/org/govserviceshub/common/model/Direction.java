package org.govserviceshub.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Gabriel.BUDAU on 30-Mar-17.
 */
@Entity
public class Direction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    @JoinTable(name = "direction_train", joinColumns = @JoinColumn(name = "direction", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "train", referencedColumnName = "id"))
    private List<Train> trainList;

    @ManyToOne
    private Station departureStation;

    @ManyToOne
    private Station arrivalStation;

    @OneToMany(mappedBy = "direction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Route> routeList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Train> getTrainList() {
        return trainList;
    }

    public void setTrainList(List<Train> trainList) {
        this.trainList = trainList;
    }

    public Station getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(Station departureStation) {
        this.departureStation = departureStation;
    }

    public Station getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(Station arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public List<Route> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }
}
