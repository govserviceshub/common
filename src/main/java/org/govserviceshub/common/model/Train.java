package org.govserviceshub.common.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Gabriel.BUDAU on 30-Mar-17.
 */
@Entity
public class Train implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long number;

    @ManyToMany(mappedBy = "trainList", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Direction> directionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<Direction> getDirectionList() {
        return directionList;
    }

    public void setDirectionList(List<Direction> directionList) {
        this.directionList = directionList;
    }
}
