package org.govserviceshub.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Gabriel.BUDAU on 30-Mar-17.
 */
@Entity
public class Station implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long code;

    private String name;

    @OneToMany(mappedBy = "departure", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Route> departureRouteList;

    @OneToMany(mappedBy = "arrival", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Route> arrivalRouteList;

    @OneToMany(mappedBy = "departureStation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Direction> departureDirectionList;

    @OneToMany(mappedBy = "arrivalStation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Direction> arrivalDirectionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Route> getDepartureRouteList() {
        return departureRouteList;
    }

    public void setDepartureRouteList(List<Route> departureRouteList) {
        this.departureRouteList = departureRouteList;
    }

    public List<Route> getArrivalRouteList() {
        return arrivalRouteList;
    }

    public void setArrivalRouteList(List<Route> arrivalRouteList) {
        this.arrivalRouteList = arrivalRouteList;
    }

    public List<Direction> getDepartureDirectionList() {
        return departureDirectionList;
    }

    public void setDepartureDirectionList(List<Direction> departureDirectionList) {
        this.departureDirectionList = departureDirectionList;
    }

    public List<Direction> getArrivalDirectionList() {
        return arrivalDirectionList;
    }

    public void setArrivalDirectionList(List<Direction> arrivalDirectionList) {
        this.arrivalDirectionList = arrivalDirectionList;
    }
}
